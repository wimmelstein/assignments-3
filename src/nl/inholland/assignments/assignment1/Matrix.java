package nl.inholland.assignments.assignment1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Matrix {

    public void start() {

        System.out.println("======= Assigment 2 =======");

        List<String> letters = new ArrayList<>();

        // Fill list with capital letters
        for (int i = 65; i < 91; i++) {
            letters.add(String.valueOf((char) i));
        }

        extractTwoRandomLetters(letters);

        // Add the list to itself, creating a list with pairs
        letters.addAll(letters);

        Collections.shuffle(letters);

        // Add a space in the middle
        letters.add(letters.size() / 2, " ");

        for (int i = 0; i < letters.size(); i++) {
            if (i % 7 == 0 && i > 0) {
                System.out.println();
            }
            System.out.print(String.format(" %s", letters.get(i)));
        }
        System.out.println();
    }

    private void extractTwoRandomLetters(List<String> letters) {
        Random random = new Random();
        letters.remove(random.nextInt(25));
        letters.remove(random.nextInt(25));
    }
}
