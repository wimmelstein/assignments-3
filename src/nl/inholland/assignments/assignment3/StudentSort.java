package nl.inholland.assignments.assignment3;


import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class StudentSort {

    List<Student> students;

    public void start() {

        System.out.println("======= Assigment 3 =======");
        this.populateStudents();
        Collections.sort(students);
        System.out.println(students);

    }

    private void populateStudents() {

        students = Arrays.asList(
                new Student("Johnson", 374),
                new Student("Smith", 400),
                new Student("Hawking", 225)
        );
    }
}
