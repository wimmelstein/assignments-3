package nl.inholland.assignments.assignment3;

public class Student implements Comparable {

    private String lastName;
    private int gpa;

    public Student(String lastName, int gpa) {
        this.lastName = lastName;
        this.gpa = gpa;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getGpa() {
        return gpa;
    }

    public void setGpa(int gpa) {
        this.gpa = gpa;
    }

    @Override
    public String toString() {
        return "Student{" +
                "lastName='" + lastName + '\'' +
                ", gpa=" + gpa +
                '}';
    }

    @Override
    public int compareTo(Object o) {
        Student other = (Student)o;
        return  this.getGpa() - ((Student)o).getGpa();
    }
}
