package nl.inholland.assignments.assignment2;

import java.util.*;

public class WordMap {

    List<String> words = Arrays.asList(
            "profligate",
            "roll",
            "fulsome",
            "glut",
            "thwart",
            "odious",
            "irrevocable",
            "luminary",
            "evoke",
            "quiescent"
    );

    public void start() {
        System.out.println("======= Assigment 1 =======");
        Map<Integer, List<String>> wordMap = new HashMap<>();
        for (String word : words) {
            if (mapDoesNotContainWordSize(wordMap, word)) {
                List<String> list = new ArrayList<>();
                list.add(word);
                wordMap.put(word.length(), list);
            } else {
                List<String> list = wordMap.get(word.length());
                list.add(word);
            }
        }

       for (Map.Entry<Integer, List<String>> entry : wordMap.entrySet()) {
           System.out.println(entry.getKey() + " letters: " + entry.getValue());
       }
    }

    private boolean mapDoesNotContainWordSize(Map<Integer, List<String>> wordMap, String word) {
        return !wordMap.containsKey(word.length());
    }

}
