package nl.inholland.assignments;

import nl.inholland.assignments.assignment1.Matrix;
import nl.inholland.assignments.assignment2.WordMap;
import nl.inholland.assignments.assignment3.StudentSort;

public class Assignment {

    public static void main(String[] args) {

        new WordMap().start();
        new Matrix().start();
        new StudentSort().start();
    }
}
